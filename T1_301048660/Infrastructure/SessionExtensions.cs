﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace T1_301048660.Infrastructure
{
    public static class SessionExtensions
    {
        public static void SetJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
        public static T GetJson<T>(this ISession session, string key)
        {
            var sessionData = session.GetString(key);
            return sessionData == null ? 
                                  default(T) : 
                                  JsonConvert.DeserializeObject<T>(sessionData);

        }

        public static List<T> GetJsonList<T>(this ISession session, string key)
        {
            var sessionData = session.GetString(key);
            return sessionData == null ?
                                  default(List<T>) :
                                  JsonConvert.DeserializeObject<List<T>>(sessionData);

        }

    }
}
