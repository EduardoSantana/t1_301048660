﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T1_301048660.Models.Poco;
using T1_301048660.Models.Repositories;
using T1_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace T1_301048660.Controllers
{
    public class CarController : Controller
    {
        private ICarRepository _carRepository { get; }

        public CarController(ICarRepository carRepository) => _carRepository = carRepository; 

        public IActionResult Index()
        {
            return View(_carRepository.Cars);
        }

        public IActionResult Details(int? id, bool created = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = _carRepository.Get(id.Value);

            if (car == null)
            {
                return NotFound();
            }

            if (created)
            {
                ViewBag.CurrentAction = "Created";
            }

            return View(car);
        }

        public IActionResult Create()
        {
            var car = new CarCreateEditViewModel();
            return View(car);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CarCreateEditViewModel car)
        {
            if (ModelState.IsValid)
            {
                int id = _carRepository.Add(Map(car));
                return RedirectToAction(nameof(Details), new { id, created = true });
            }
            return View(car);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = _carRepository.Get(id.Value);

            if (car == null)
            {
                return NotFound();
            }

            return View(Map(car));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, CarCreateEditViewModel car)
        {
            if (id != car.CarID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _carRepository.Update(Map(car));
                }
                catch (Exception ex)
                {
                    if (!CarExists(car.CarID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = _carRepository.Get(id.Value);

            if (car == null)
            {
                return NotFound();
            }

            return View(car);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _carRepository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool CarExists(int id)
        {
            return _carRepository.Cars.Any(e => e.CarID == id);
        }
        private CarCreateEditViewModel Map(Car car)
        {
            return new CarCreateEditViewModel()
            {
                CarID = car.CarID,
                LicencePlate = car.LicencePlate,
                OwnersName = car.OwnersName,
                OwnersAddress = car.OwnersAddress,
                Make = car.Make,
                Model = car.Model,
                AD4Doors = car.AD4Doors,
                AD4WD = car.AD4WD,
                ADHybrid = car.ADHybrid
            };
        }
        private Car Map(CarCreateEditViewModel car)
        {
            return new Car()
            {
                CarID = car.CarID,
                LicencePlate = car.LicencePlate,
                OwnersName = car.OwnersName,
                OwnersAddress = car.OwnersAddress,
                Make = car.Make,
                Model = car.Model,
                AD4Doors = car.AD4Doors,
                AD4WD = car.AD4WD,
                ADHybrid = car.ADHybrid
            };
        }

    }
}