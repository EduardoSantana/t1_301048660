﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1_301048660.Models.Poco
{
    public class Car
    {
        public int CarID { get; set; }
        public string LicencePlate { get; set; }
        public string OwnersName { get; set; }
        public string OwnersAddress { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public bool AD4Doors { get; set; }
        public bool ADHybrid { get; set; }
        public bool AD4WD { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
