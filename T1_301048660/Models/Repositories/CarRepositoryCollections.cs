﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using T1_301048660.Models.Poco;
using T1_301048660.Infrastructure;

namespace T1_301048660.Models.Repositories
{
    public class CarRepositoryCollections : ICarRepository
    {

        public CarRepositoryCollections(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
            cars = _httpContext.HttpContext.Session.GetJsonList<Car>("Cars") ?? new List<Car>();
        }

        private readonly IHttpContextAccessor _httpContext;

        private List<Car> cars { get; set; }

        private void SaveCars(List<Car> c) => _httpContext.HttpContext.Session.SetJson("Cars", c);

        public IQueryable<Car> Cars => cars.OrderBy(p=>p.OwnersName).AsQueryable<Car>();

        public int Add(Car car)
        {
            int id = 1;
            if (cars.Any())
                id += cars.Max(p=> p.CarID);

            car.CarID = id;
            car.ModifiedDate = DateTime.Now;
            cars.Add(car);
            SaveCars(cars);
            return id;
        }
            
        public void Update(Car car)
        {
            var ob = cars.Where(p => p.CarID == car.CarID).FirstOrDefault();
            ob.LicencePlate = car.LicencePlate;
            ob.OwnersAddress = car.OwnersAddress;
            ob.Make = car.Make;
            ob.Model= car.Model;
            ob.AD4Doors = car.AD4Doors;
            ob.AD4WD = car.AD4WD;
            ob.ADHybrid = car.ADHybrid;
            ob.ModifiedDate = DateTime.Now;
            SaveCars(cars);
        }

        public void Delete(int id)
        {
            cars.RemoveAll(p => p.CarID == id);
            SaveCars(cars);
        }

        public Car Get(int id) => cars.Where(p => p.CarID == id).FirstOrDefault();
    }
}
