﻿using T1_301048660.Models.Poco;
using System.Linq;

namespace T1_301048660.Models.Repositories
{
    public interface ICarRepository
    {
        IQueryable<Car> Cars { get; }
        int Add(Car car);
        void Update(Car car);
        void Delete(int id);
        Car Get(int id);
    }
}
