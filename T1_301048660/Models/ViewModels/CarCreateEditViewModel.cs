﻿using T1_301048660.Models.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace T1_301048660.Models.ViewModels
{
    public class CarCreateEditViewModel
    {
        public CarCreateEditViewModel()
        {
            CarID = -1;
            Makes.Add(new SelectListItem { Value = "", Text = "-- Select --" });
            Makes.Add(new SelectListItem { Value = "Honda", Text = "Honda" });
            Makes.Add(new SelectListItem { Value = "Toyota", Text = "Toyota" });
            Makes.Add(new SelectListItem { Value = "Volvo", Text = "Volvo" });
            Makes.Add(new SelectListItem { Value = "Lexus", Text = "Lexus" });
        }
        public int CarID { get; set; }
        
        public string LicencePlate { get; set; }
        public string OwnersName { get; set; }
        public string OwnersAddress { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public bool AD4Doors { get; set; }
        public bool ADHybrid { get; set; }
        public bool AD4WD { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public List<SelectListItem> Makes = new List<SelectListItem>();
    }
}
